<?php
namespace Grobmeier\PHPUnit;

use Inventory\Services\InventoryStockService;
use Inventory\Models\Products;

class InventoryStockServiceTest extends \PHPUnit_Framework_TestCase
{   
    public $inventoryStockService;
    
    protected function setUp()
    {   
        $this->inventoryStockService = new InventoryStockService();
        $GLOBALS['product_stocks'][Products::LAMINGTON] = 5;
        
    }
    
    /** @test */
	public function inventoryShouldReturnProductStocks()
	{   
        
        $productId = 2;
		$result = $this->inventoryStockService->getStockLevel($productId);
		$this->assertEquals(5, $result);
    }
    
    /** @test */
	public function inventoryShouldReturnZeroIfProductIdNotExists()
	{   
        
        $productId = 10;
		$result = $this->inventoryStockService->getStockLevel($productId);
		$this->assertEquals(0, $result);
    }
    
    /** @test */
    public function shouldReturnExceptionIfEmptyProductId()
    {   
        $this->expectException(\InvalidArgumentException::class);
        $result = $this->inventoryStockService->getStockLevel(null);
       
    }

    /** @test */
    public function shouldReturnTrueIfOrderMetTheStocks()
    {
        $orderData = array(
            Products::LAMINGTON => 5
        );
        $result = $this->inventoryStockService->isStocksVsOrderMet($orderData);
        $this->assertTrue($result);
    }

    /** @test */
    public function shouldReturnFalseIfOrderNotMetTheStocks()
    {
        $orderData = array(
            Products::LAMINGTON => 500
        );
        $result = $this->inventoryStockService->isStocksVsOrderMet($orderData);
        $this->assertFalse($result);
    }
    
}
