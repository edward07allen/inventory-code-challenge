<?php
namespace Grobmeier\PHPUnit;

use Inventory\Services\ProductPurchaseService;
use Inventory\Models\Products;

class ProductPurchaseServiceTest extends \PHPUnit_Framework_TestCase
{   
    public $productPurchaseService; 

    protected function setUp()
    {   
        $this->productPurchaseService = new ProductPurchaseService();
        //set globals value 
        $GLOBALS['product_stocks'][Products::LAMINGTON] = 5;
        $GLOBALS['product_sold_history'] = array();
        $GLOBALS['product_purchase'] = array(
            array(
                "day_purchase" => 1,
                "productId" => 1,
                "quantity" => 20,
                "receive_by" => 3,
                "status" => "receive",
                "day_recieve"=> 3
            ),
            array(
                "day_purchase" => 1,
                "productId" => 3,
                "quantity" => 20,
                "receive_by" => 3,
                "status" => "receive",
                "day_recieve"=> 3
            ),
            array(
                "day_purchase" => 3,
                "productId" => 3,
                "quantity" => 20,
                "receive_by" => 5,
                "status" => "pending",
                "day_recieve"=> 0
            ),
            array(
                "day_purchase" => 4,
                "productId" => 3,
                "quantity" => 20,
                "receive_by" => 6,
                "status" => "pending",
                "day_recieve"=> 0
            ),
        );
        
    }
    
    /** @test */
    public function shouldReturnNullIfProductIdIsNull()
    {
        $result = $this->productPurchaseService->purchaseProduct(0, 10, 1);
        $this->assertNull($result);
    } 

    /** @test */
    public function shouldReturnNullIfQuantityIsNull()
    {
        $result = $this->productPurchaseService->purchaseProduct(2, 0, 1);
        $this->assertNull($result);
    } 

    /** @test */
    public function shouldReturnNullIfOrderQuantityNOtMetTheStocks()
    {
        $result = $this->productPurchaseService->purchaseProduct(Products::LAMINGTON, 10, 1);
        $this->assertNull($result);
    } 

    /** @test */
    public function onPurchaseSuccessItShouldAddDataInProductSoldHistoryAndDeductThePurchaseUQuantityInStocks()
    {
        $result = $this->productPurchaseService->purchaseProduct(Products::LAMINGTON, 2, 1);
        
        // history should write in globals
        $this->assertNotEmpty($GLOBALS['product_sold_history']);

        // product_stocks should be deducted
        $this->assertEquals(3, $GLOBALS['product_stocks'][Products::LAMINGTON]);
    } 


    /** @test */
    public function shouldReturnQuantityOfProductIdThatIsForPendingPurchasing()
    {
        $result = $this->productPurchaseService->getPurchasedPendingTotal(Products::BLUEBERRY_MUFFIN);
        
        //should return 40
        $this->assertEquals(40, $result);
    }

    /** @test */
    public function shouldReturnQuantityOfProductIdThatIsAlreadyReceived()
    {
        $result = $this->productPurchaseService->getPurchasedReceivedTotal(Products::BROWNIE);
        
        //should return 20
        $this->assertEquals(20, $result);
    }

}