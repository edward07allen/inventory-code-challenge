<?php
namespace Grobmeier\PHPUnit;

use Inventory\Services\ProductSoldService;
use Inventory\Models\Products;

class ProductSoldServiceTest extends \PHPUnit_Framework_TestCase
{   
    public $productSoldService; 

    protected function setUp()
    {   
        $this->productSoldService = new ProductSoldService();
        $GLOBALS['product_sold_history'] = array(
            0 => '{"day":0,"productId":1,"quantity":2}',
            1 => '{"day":0,"productId":1,"quantity":1}',
            2 => '{"day":0,"productId":5,"quantity":2}'
        );
        
    }

    /** @test */
	public function shouldReturnTheQuantitySoldByGivenProductId()
	{   
        // test for productId 1
        $result = $this->productSoldService->getSoldTotal(Products::BROWNIE);
        $this->assertGreaterThan(0, $result);
    
    }

    /** @test */
	public function soldQuantityShouldBeEqualToSoldValueFromSetUp()
	{   
        // test for productId 5
        $result = $this->productSoldService->getSoldTotal(Products::CHOCOLATE_CAKE);
        $this->assertEquals(2 , $result);
    }

    /** @test */
	public function shouldReturnZeroIfProductDoesNotSoldAnyQuantity()
	{   
        // test for productId 5
        $result = $this->productSoldService->getSoldTotal(Products::CROISSANT);
        $this->assertEquals(0 , $result);
    }

}