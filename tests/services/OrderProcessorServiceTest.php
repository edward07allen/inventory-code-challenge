<?php
namespace Grobmeier\PHPUnit;

use Inventory\Services\OrderProcessorService;
use Inventory\Services\ProductPurchaseService;
use Inventory\Services\InventoryStockService;
use Inventory\Models\Products;

class OrderProcessorServiceTest extends \PHPUnit_Framework_TestCase
{   

    public $orderProcessorService; 

    protected function setUp()
    {   
        $this->orderProcessorService = new OrderProcessorService(
            new InventoryStockService(),
            new ProductPurchaseService(), 
        );
        $GLOBALS['product_stocks'][Products::BLUEBERRY_MUFFIN] = 5;
        $GLOBALS['product_stocks'][Products::BROWNIE] = 15;
        $GLOBALS['product_stocks'][Products::CROISSANT] = 9;
        $GLOBALS['product_stocks'][Products::CHOCOLATE_CAKE] = 20;
        
        $GLOBALS['product_purchase'] = array(
            array(
                "day_purchase" => 1,
                "productId" => 1,
                "quantity" => 20,
                "receive_by" => 3,
                "status" => "receive",
                "day_recieve"=> 3
            ),
            array(
                "day_purchase" => 1,
                "productId" => 3,
                "quantity" => 20,
                "receive_by" => 3,
                "status" => "receive",
                "day_recieve"=> 3
            ),
            array(
                "day_purchase" => 1,
                "productId" => 5,
                "quantity" => 20,
                "receive_by" => 5,
                "status" => "pending",
                "day_recieve"=> 3
            ),
            array(
                "day_purchase" => 3,
                "productId" => 3,
                "quantity" => 20,
                "receive_by" => 5,
                "status" => "pending",
                "day_recieve"=> 0
            )
        );
    }

    protected static function getMethod($obj, $name) {
        $class = new \ReflectionClass(get_class($obj));
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
      }

    /** @test */
    public function shouldThrowExceptionIfFileNotFound()
    {   
        $this->expectException(\InvalidArgumentException::class);
        $filePath = dirname(dirname(__DIR__)) . '\orders-sample-not-exist.json';
        $this->orderProcessorService->processFromJson($filePath);
    }
    
    /** @test */
    public function shouldReceivePurchaseProduct()
    {   
        $initialProductPurchase = $GLOBALS['product_purchase'];
        $day = 5;
        $protectedMethod = self::getMethod($this->orderProcessorService, 'receiveStocks');
        $protectedMethod->invokeArgs($this->orderProcessorService, array($day));
        
        $this->assertNotEquals($initialProductPurchase , $GLOBALS['product_purchase']);
        
        //stocks should update and will be 25 
        $this->assertEquals(25, $GLOBALS['product_stocks'][Products::BLUEBERRY_MUFFIN]);
    
    }

    /** @test */
    public function shouldNotReceivePurchaseProduct()
    {  
        
        $initialProductPurchase = $GLOBALS['product_purchase'];
        $day = 1;
        $protectedMethod = self::getMethod($this->orderProcessorService, 'receiveStocks');
        $protectedMethod->invokeArgs($this->orderProcessorService, array($day));
        
        // product purchase should be the same
        $this->assertEquals($initialProductPurchase , $GLOBALS['product_purchase']);
    }

    /** @test */
    public function shouldCreatePurchaseOrderIfItemIsLowStocks()
    {
        $day = 4;
        $initialProductPurchase = $GLOBALS['product_purchase'];
        $protectedMethod = self::getMethod($this->orderProcessorService, 'checkLowStockItems');
        $protectedMethod->invokeArgs($this->orderProcessorService, array($day));

        $this->assertNotEquals($initialProductPurchase , $GLOBALS['product_purchase']);
    }

    /** @test */
    public function shouldNotCreatePurchaseOrderSinceItemHasExistingPurchase()
    {   
        $day = 4;
        $GLOBALS['product_stocks'][Products::CHOCOLATE_CAKE] = 7;
        $GLOBALS['product_stocks'][Products::CROISSANT] = 10;
        $initialProductPurchase = $GLOBALS['product_purchase'];
        $protectedMethod = self::getMethod($this->orderProcessorService, 'checkLowStockItems');
        $protectedMethod->invokeArgs($this->orderProcessorService, array($day));
    
        $this->assertEquals($initialProductPurchase , $GLOBALS['product_purchase']);
    }

    /** @test */
    public function shouldReturnTrueIfHavePendingPurchase()
    {   
        $protectedMethod = self::getMethod($this->orderProcessorService, 'hasPendingPurchase');
        $result = $protectedMethod->invokeArgs($this->orderProcessorService, array(Products::CHOCOLATE_CAKE));
        
        $this->assertTrue($result);  
    }

    /** @test */
    public function shouldReturnFalseIfDontHavePendingPurchase()
    {   
        $protectedMethod = self::getMethod($this->orderProcessorService, 'hasPendingPurchase');
        $result = $protectedMethod->invokeArgs($this->orderProcessorService, array(Products::BROWNIE));
        
        $this->assertFalse($result);  
    }


}