<?php
namespace Inventory\Interfaces;

interface ProductsSoldInterface
{
    /**
     * Get Total Sold Per Product
     * @param int $productId
     * @return int
     */
    public function getSoldTotal(int $productId): int;
}
