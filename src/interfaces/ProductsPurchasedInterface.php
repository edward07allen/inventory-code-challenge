<?php
namespace Inventory\Interfaces;

interface ProductsPurchasedInterface
{
    /**
     * Get Purchased Received Total Per Product
     * @param int $productId
     * @return int
     */
    public function getPurchasedReceivedTotal(int $productId): int;

    /**
     * Get Purchased Pending Total Per Product
     * @param int $productId
     * @return int
     */
    public function getPurchasedPendingTotal(int $productId): int;

    /**
     * Pruchase a Product
     * @param int $productId, int $quantity, int $day
     *
     */
    public function purchaseProduct(int $productId, int $quantity, int $day);

}
