<?php
namespace Inventory\Interfaces;

interface InventoryInterface
{
    /**
     * Get current Stock Level
     * @param int $productId
     * @return int
     */
    public function getStockLevel(int $productId): int;

    /**
     * Check if order met stocks value
     * @param array $order
     * @return boolean
     */
    public function isStocksVsOrderMet(array $order);
}
