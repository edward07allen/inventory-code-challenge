<?php 
namespace Inventory\Services;

use Inventory\Interfaces\OrderProcessorInterface;
class OrderProcessorService implements OrderProcessorInterface {

    protected $inventoryStockService;
    protected $productPurchaseService;
    
    public function __construct($InventoryStockService, $ProductPurchaseService) {
        $this->inventoryStockService = $InventoryStockService;
        $this->productPurchaseService = $ProductPurchaseService;
    }

    /**
     * Process Orders from JSON Data
     * @param int $productId
     */
    public function processFromJson($filePath):void 
    {
        
        // check if file exists
        if (!file_exists($filePath)) {
            throw new \InvalidArgumentException("File is is  required");
        }
        
        // get json orders
        $jsonDataOrders = json_decode(file_get_contents($filePath), true);
      
        //check if have orders
        if (!empty($jsonDataOrders)) {
            // loop through daily orders
            foreach ($jsonDataOrders as $day => $orders) {
                
                // receive stocks for pending order purchase
                $this->receiveStocks($day);

                // process daily orders
                $this->processDailyOrders($day, $orders);
                
                // check for low stock items
                $this->checkLowStockItems($day);
            }    
        }
    }

    /**
     * Receive Stocks by Given Day
     * @param int $productId
     */
    protected function receiveStocks($day)
    {  
        $productPendingPurchase = $GLOBALS['product_purchase'];
        $productPendingPurchaseNewData = array();
        if (!empty($productPendingPurchase)) {
            foreach ($productPendingPurchase as $pendingPurchase) {
                if ($pendingPurchase['receive_by'] == $day) {
                    // add quantity to product stocks
                    $productQuantity = $GLOBALS['product_stocks'][$pendingPurchase['productId']];
                    $pendingPurchase['status'] = 'receive';
                    $pendingPurchase['day_recieve'] = $day;
                   
                    $GLOBALS['product_stocks'][$pendingPurchase['productId']] = $productQuantity + $pendingPurchase['quantity'];
                }
                 array_push($productPendingPurchaseNewData, $pendingPurchase);
            }
            $GLOBALS['product_purchase'] = $productPendingPurchaseNewData;
        }
    
    }

    /**
     * Pruchase product if current stocks are low
     * @param int $productId
     */
    protected function checkLowStockItems($day) 
    {
        $productStocks = $GLOBALS['product_stocks'];
        foreach ($productStocks as $productId => $stockQuantity) {
            if ($stockQuantity < 10) {
                //check for pending purchase
                if (!$this->hasPendingPurchase($productId)) {
                    $productPendingPurchase = $GLOBALS['product_purchase'];
                    // create product purchase
                    $receiveBy = ($day + 2) > 6 ? 0 : ($day + 2);
                    array_push($productPendingPurchase, array(
                            'day_purchase'       => $day,
                            'productId' => $productId,
                            'quantity'  => 20,
                            'receive_by' => $receiveBy,
                            'status'    => 'pending',
                            'day_recieve' => ''
                        )
                    );
                    //set new value for global product_pending_purchase
                    $GLOBALS['product_purchase'] = $productPendingPurchase;
                }
            }
        }
    }

    /**
     * Check if product has pending purchase
     * @param int $productId
     */
    protected function hasPendingPurchase($productId) 
    {   
        //check for product id
        if (!empty($productId)) {
            $productPurchase = $GLOBALS['product_purchase'];
            foreach($productPurchase as $purchase) {
                // check for pruchase history
                if ($purchase['productId'] == $productId && $purchase['status'] == 'pending') {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Process daily orders
     * @param $orders
     */
    public function processDailyOrders($day, $orders)
    {
        if (!empty($orders) && is_array($orders)) {
            // loop through orders
            foreach ($orders as $order) {
                // loop through order items
                if (!empty($order)) {
                   // check if order met product stocks
                    if ($this->inventoryStockService->isStocksVsOrderMet($order)) {
                        // purchase items
                        foreach ($order as $productId => $quantity) {
                            $this->productPurchaseService->purchaseProduct($productId, $quantity, $day);
                        }  
                    } 
                }
            }
        }
    }

}