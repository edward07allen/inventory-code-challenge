<?php 
namespace Inventory\Services;


use Inventory\Interfaces\ProductsPurchasedInterface;

class ProductPurchaseService implements ProductsPurchasedInterface {

    /**
     * Get Purchased Total Per Product
     * @param int $productId
     */
    public function getPurchasedReceivedTotal($productId): int {
        $totalProductReceive = 0;
        foreach ($GLOBALS['product_purchase'] as $purchase) {
           if ($purchase['status'] == 'receive' && $purchase['productId'] == $productId) {
               $totalProductReceive = $totalProductReceive + $purchase['quantity'];
           }
        }
        return $totalProductReceive;
    }

    /**
     * Get Purchased Total which is pending
     * @param int $productId
     */
    public function getPurchasedPendingTotal($productId): int {
       
        $totalProductPending = 0;
        foreach ($GLOBALS['product_purchase'] as $purchase) {
           if ($purchase['status'] == 'pending' && $purchase['productId'] == $productId) {
               $totalProductPending = $totalProductPending + $purchase['quantity'];
           }
        }
        return $totalProductPending;
    }

    /**
     * Pruchase product
     * @param int $productId
     */
    public function purchaseProduct(int $productId, int $quantity, $day) {
        
        if (!empty($productId) && !empty($quantity)) {
            //get product stocks value
            $productStocksValue = !empty($GLOBALS['product_stocks'][$productId]) ? $GLOBALS['product_stocks'][$productId] : 0;
            if (!empty($productStocksValue) && $productStocksValue >= $quantity) {
                // deduct quantity from stocks value
                $GLOBALS['product_stocks'][$productId] = $productStocksValue - $quantity;
                // set product_purchase history
                $purchaseHistory = $GLOBALS['product_sold_history'];
                array_push($purchaseHistory, json_encode(
                    array(
                        'day'       => $day,
                        'productId' => $productId,
                        'quantity'  => $quantity,
                    )
                ));

                //set new value for global product_sold_history
                $GLOBALS['product_sold_history'] = $purchaseHistory;
            }
        }
    }

}