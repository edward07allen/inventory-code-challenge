<?php 
namespace Inventory\Services;

use Inventory\Interfaces\InventoryInterface;
class InventoryStockService implements InventoryInterface {

    /**
     * Get Current Stock Level Per Product
     * @param $orders
     */
    public function getStockLevel($productId): int {

        if (empty($productId)) {
            throw new \InvalidArgumentException("Product Id is  required");
        }

        //get product stocks
        if (!empty($GLOBALS['product_stocks'][$productId])) {
            return $GLOBALS['product_stocks'][$productId];
        }
        
        return 0;
    }

    /**
     * Check if Order met Stocks Values
     * @param $orders
     */
    public function isStocksVsOrderMet($order) {
        
        //if order is not empty
        if (!empty($order)) {
            foreach ($order as $productId => $quantity) {
                $stockLevelValue = $this->getStockLevel($productId);
                // check if product met quantity values
                if (!($stockLevelValue >= $quantity) || empty($stockLevelValue)) {
                    return false;
                } 
            }
        }
        return true;
    }
}