<?php 
namespace Inventory\Services;

use Inventory\Interfaces\ProductsSoldInterface;

class ProductSoldService implements ProductsSoldInterface {

    /**
     * Get Sold Total per Product Id
     * @param int $productId
     */
    public function getSoldTotal($productId): int {
        $totalProductSold = 0;
        foreach ($GLOBALS['product_sold_history'] as $productsSold) {
            $productSoldData = json_decode($productsSold, true);
            if ($productSoldData['productId'] == $productId) {
                $totalProductSold = $totalProductSold + $productSoldData['quantity'];
            }
        }
        return $totalProductSold;
    }

}