<?php
    require 'autoload.php';

    use Inventory\Services\InventoryStockService;
    use Inventory\Services\OrderProcessorService;
    use Inventory\Services\ProductPurchaseService;
    use Inventory\Services\ProductSoldService;
    use Inventory\Models\Products;
    
    // first set the initial 20 for all products and storage needed
    $GLOBALS = array(
        'product_stocks' => array(
            Products::BROWNIE => 20,
            Products::LAMINGTON => 20,
            Products::BLUEBERRY_MUFFIN => 20,
            Products::CROISSANT => 20,
            Products::CHOCOLATE_CAKE => 20                          
        ),
        'product_sold_history' => array(),
        'product_purchase' => array(),
    );

    $productLabel = array(
        Products::BROWNIE => 'BROWNIE',
        Products::LAMINGTON => 'LAMINGTON',
        Products::BLUEBERRY_MUFFIN => 'BLUEBERRY_MUFFIN',
        Products::CROISSANT => 'CROISSANT',
        Products::CHOCOLATE_CAKE => 'CHOCOLATE_CAKE'  
    );
  
    //process order from txt file
    $orderProcessorService = new OrderProcessorService(
        new InventoryStockService(),
        new ProductPurchaseService()
    );
    $orderProcessorService->processFromJson(__DIR__ . '/orders-sample.json');

   
    $productSoldService = new ProductSoldService();
    $productPurchaseService = new ProductPurchaseService();

    //$mask = "|%-35.30s |%-30.30s | x |\n";
    $mask = "|%-40.40s| %-20.20s | %-20.30s | %-20.30s | %-20.30s | %-20.30s |\n";
    // header
    printf($mask,
        '',
        $productLabel[Products::BROWNIE],
        $productLabel[Products::LAMINGTON],
        $productLabel[Products::BLUEBERRY_MUFFIN],
        $productLabel[Products::CROISSANT],
        $productLabel[Products::CHOCOLATE_CAKE]
    );
    // total sold
    printf(
        $mask, 
        'Total Units Sold', 
        $productSoldService->getSoldTotal(Products::BROWNIE),
        $productSoldService->getSoldTotal(Products::LAMINGTON),
        $productSoldService->getSoldTotal(Products::BLUEBERRY_MUFFIN),
        $productSoldService->getSoldTotal(Products::CROISSANT),
        $productSoldService->getSoldTotal(Products::CHOCOLATE_CAKE) 
    );
    // total units purchased and pending
    printf(
        $mask, 
        'Total Units Purchased and Pending', 
        $productPurchaseService->getPurchasedPendingTotal(Products::BROWNIE),
        $productPurchaseService->getPurchasedPendingTotal(Products::LAMINGTON),
        $productPurchaseService->getPurchasedPendingTotal(Products::BLUEBERRY_MUFFIN),
        $productPurchaseService->getPurchasedPendingTotal(Products::CROISSANT),
        $productPurchaseService->getPurchasedPendingTotal(Products::CHOCOLATE_CAKE) 
    );   
    // total units purchased and received
    printf(
        $mask, 
        'Total Units Purchased and Received', 
        $productPurchaseService->getPurchasedReceivedTotal(Products::BROWNIE),
        $productPurchaseService->getPurchasedReceivedTotal(Products::LAMINGTON),
        $productPurchaseService->getPurchasedReceivedTotal(Products::BLUEBERRY_MUFFIN),
        $productPurchaseService->getPurchasedReceivedTotal(Products::CROISSANT),
        $productPurchaseService->getPurchasedReceivedTotal(Products::CHOCOLATE_CAKE) 
    ); 
    // Current Stock level
    printf(
        $mask, 
        'Current Stock Level', 
        $GLOBALS['product_stocks'][Products::BROWNIE],
        $GLOBALS['product_stocks'][Products::LAMINGTON],
        $GLOBALS['product_stocks'][Products::BLUEBERRY_MUFFIN],
        $GLOBALS['product_stocks'][Products::CROISSANT],
        $GLOBALS['product_stocks'][Products::CHOCOLATE_CAKE] 
    );   
    
?>