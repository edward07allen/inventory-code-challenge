<?php
    require 'autoload.php';

    use Inventory\Services\InventoryStockService;
    use Inventory\Services\OrderProcessorService;
    use Inventory\Services\ProductPurchaseService;
    use Inventory\Services\ProductSoldService;
    use Inventory\Models\Products;
    
    // first set the initial 20 for all products and storage needed
    $GLOBALS = array(
        'product_stocks' => array(
            Products::BROWNIE => 20,
            Products::LAMINGTON => 20,
            Products::BLUEBERRY_MUFFIN => 20,
            Products::CROISSANT => 20,
            Products::CHOCOLATE_CAKE => 20                          
        ),
        'product_sold_history' => array(),
        'product_purchase' => array(),
    );

    $productLabel = array(
        Products::BROWNIE => 'BROWNIE',
        Products::LAMINGTON => 'LAMINGTON',
        Products::BLUEBERRY_MUFFIN => 'BLUEBERRY_MUFFIN',
        Products::CROISSANT => 'CROISSANT',
        Products::CHOCOLATE_CAKE => 'CHOCOLATE_CAKE'  
    );
  
    //process order from txt file
    $orderProcessorService = new OrderProcessorService(
        new InventoryStockService(),
        new ProductPurchaseService()
    );
    $orderProcessorService->processFromJson(__DIR__ . '/orders-sample.json');

   
    $productSoldService = new ProductSoldService();
    $productPurchaseService = new ProductPurchaseService();
?>
<html>
    <head>
    <link rel="stylesheet" href="assets/styles.css">
    </head>
    <body>
        <h3>1 Week Trading Summary</h3>
        <table id="inventory">
            <thead>
                <tr>
                    <th></th>
                    <?php foreach($GLOBALS['product_stocks'] as $productId => $product) { ?>
                        <th><b><?=$productLabel[$productId]?></th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Total Units Sold</td>
                    <?php foreach($GLOBALS['product_stocks'] as $productId => $product) { ?>
                        <td><?= $productSoldService->getSoldTotal($productId) ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td>Total Units Purchased and Pending</td>
                    <?php foreach($GLOBALS['product_stocks'] as $productId => $product) { ?>
                        <td><?= $productPurchaseService->getPurchasedPendingTotal($productId) ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td>Total Units Purchased and Received</td>
                    <?php foreach($GLOBALS['product_stocks'] as $productId => $product) { ?>
                        <td><?= $productPurchaseService->getPurchasedReceivedTotal($productId) ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td>Current Stock Level</td>
                    <?php foreach($GLOBALS['product_stocks'] as $productId => $product) { ?>
                        <td><?= $product ?></td>
                    <?php } ?>
                </tr>
            </tbody>
        </table>
    </body>    
</html>