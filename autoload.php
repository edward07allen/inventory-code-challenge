<?php
$src = array(
    dirname(__FILE__) . "/src/interfaces",
    dirname(__FILE__) . "/src/models",
    dirname(__FILE__) . "/src/services"
);
foreach ( $src as $folders) {
    $files = glob($folders . '/*.php');
    foreach ($files as $file) {
        require($file);
    }
}